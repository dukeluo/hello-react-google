import React from 'react';
import './Lang.less';

class Lang extends React.Component {
    render() {
        return (
            <div className="lang-list">
                <ul>
                    <li>Google offered in:</li>
                    <li><a href="#">中文（繁體）</a></li>
                    <li><a href="#">中文（简体）</a></li>
                </ul>
            </div>
        );
    }
}

export default Lang;