import React from "react";
import "./Box.less";
import {MdSearch} from 'react-icons/md';

const Box = () => {
    return (
        <section className="search-box">
            <div>
                <MdSearch className='icon'/>
                <input type="text" />
            </div>
        </section>
    );
};

export default Box;