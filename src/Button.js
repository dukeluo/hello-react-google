import React from 'react';
import './Button.less';

class Button extends React.Component {
    render() {
        return <input type="button" value={this.props.value} />;
    }
}

export default Button;