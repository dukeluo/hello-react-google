import React from 'react';
import './Footer.less';

class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <div className="location">
                    <p>Hong Kong</p>
                </div>
                <div className="more">
                    <ul>
                        <li><a href="#">Advertising</a></li>
                        <li><a href="#">Business</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">How Search works</a></li>
                    </ul>
                </div>
            </footer>

        );
    }
}

export default Footer;