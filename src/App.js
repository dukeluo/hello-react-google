import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import Box from './Box';
import Button from './Button';
import Lang from './Lang'; 
import Footer from './Footer';

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
      <Box />
      <div className="button">
        <Button value="Google Search"/>
        <Button value="I'm Feeling Lucky"/>
      </div>
      <Lang />
      <Footer />
    </div>
  );
};

export default App;